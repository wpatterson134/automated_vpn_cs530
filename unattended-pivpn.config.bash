
# the VPN variable tells which VPN to use. In this case, it would choose between
# OpenVPN or WireGuard. I wanted to choose OpenVPN because I found a useful tutorial regarding OpenVPN
VPN="openvpn"

# the pivpnHost variable tells the public IP address
pivpnHost="72.197.180.237"