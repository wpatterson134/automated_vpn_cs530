#! /usr/bin/env python

# os is a module that allows you to treat python like bash
import os
# subprocess allows you to run bash commands in python
import subprocess

# sudo password is unsafe here. I will need to hide this password 
# in the future. The current goal of this is to just get my project
# up and running
sudo_password = 'asdf1234qwer' 

# echo will print out the item that comes directly after it. In this case
# I wanted to print out the password. Then, the password is sent
# to the sudo command. The sudo command then reads the password from stdin
# The stdout of echo becomes the stdin of sudo. In this way, I can
# automatically enter the password or any other item. 

# 'update' is necessary to check for the latest packages available for all
# system software
os.system(f'echo {sudo_password}|sudo -S apt update')

# 'full-upgrade' will check which versions I have installed to see if 
# there are newer versions; will install the latest versions
os.system(f'echo {sudo_password}|sudo -S apt full-upgrade')

# 'vsftpd' allows me to used FileZilla to transfer files from Raspberry Pi
# to my home laptop
os.system(f'echo {sudo_password}|sudo -S apt install vsftpd')

# 'xclip' is used for setup purposes, but is not necessary to run VPN
os.system(f'echo {sudo_password}|sudo -S apt install xclip')

# We set variables for the public ip address as well as our choice between Wireguard VPN
# and OpenVPN. These variables are found in 'unattended-pivpn.config.bash
# These variables are used to make configuration choices that were not
# automated before I added the automation steps. 'subprocess.call' calls the 
# bash file 'install.pivpn.io.bash' and tells the script to run 'unattended and use the file
# named by the last argument 'unattended-pivpn.config.bash'. I probably could have used os.system instead,
# but this was what I got working first, so I just stuck with it.  
subprocess.call(['./install.pivpn.io.bash', '--unattended', 'unattended-pivpn.config.bash'])

# I used 'wget' to download the install script 'install.pivpn.io.bash' so that I could read it find the settings that
# needed to be changed


'''
Below is the history of my Raspberry Pi terminal after I got 
my VPN running. I used this history to make a Python script that automates
the installation of the VPN onto a Raspberry Pi

sudo apt update
sudo apt full-upgrade
sudo apt install vsftpd
curl -L https://install.pivpn.io | bash
ip addr
ip link show
ip addr
pivpn add
whoami
pivpn help
'''